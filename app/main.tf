
provider "vault" {
  # This will default to using $VAULT_ADDR
  # But can be set explicitly
  address = "http://localhost:8200"
  token   = "testing123"
  ## token = we reccommend to set this as an environment variable ex. TF_VAR_token
}

terraform {
  backend "consul" {
    address = "localhost:8500"
    path    = "vault/app"
  }
}

data "terraform_remote_state" "policy" {
  backend = "consul"
  config = {
    path = "vault/policy"
  }
}


output "policy_canary" {
  value = data.terraform_remote_state.policy.outputs.app_tokens.canary-policy
}

output "policy_ilc" {
  value = data.terraform_remote_state.policy.outputs.app_tokens.ilc-policy
}

resource "vault_generic_secret" "ilc" {
  path = "secret/ilc-creds"

  data_json = <<EOT
{
  "username":   "fewknow",
  "password": "2929"
}
EOT
}

resource "vault_generic_secret" "canary" {
  path = "secret/canary-creds"

  data_json = <<EOT
{
  "username":   "nono",
  "password": "1010"
}
EOT
}
