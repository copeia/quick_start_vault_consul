resource "vault_database_secret_backend_role" "role" {
  count               = length(data.terraform_remote_state.policy.outputs.apps)
  backend             = "${data.terraform_remote_state.engine.outputs.db_mount}"
  name                = "${data.terraform_remote_state.policy.outputs.apps[count.index]}-role"
  db_name             = "${data.terraform_remote_state.engine.outputs.db_name[count.index]}"
  creation_statements = ["CREATE LOGIN [{{name}}] WITH PASSWORD = '{{password}}';",
                         "CREATE USER  [{{name}}] FOR LOGIN [{{name}}];",
                         "GRANT SELECT ON SCHEMA::dbo TO [{{name}}];"]
    
}
