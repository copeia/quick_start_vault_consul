output "app_tokens" {
  value = {
  for instance in vault_token.apps-token:
  instance.display_name => instance.client_token
      }
}

output "apps" {
  value = var.apps
}
