
terraform {
  backend "consul" {
    address = "localhost:8500"
    path    = "vault/policy"
  }
}

provider "vault" {
  # This will default to using $VAULT_ADDR
  # But can be set explicitly
  address = "http://localhost:8200"
  token   = "testing123"
  ## token = we reccommend to set this as an environment variable ex. TF_VAR_token
}
