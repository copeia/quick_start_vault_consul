#Policy to allow changes con applications available
resource "vault_policy" "apps-policy" {
  count = length(var.apps)
  name  = "${var.apps[count.index]}-policy"

  policy = <<EOT

    path "secret*" {
      capabilities = ["list"]
    }

    path "secret/data/${var.apps[count.index]}-creds*" {
      capabilities = ["read", "list"]
    }

    path "database/creds*" {
        capabilities = ["list"]
    }

    path "database/creds/${var.apps[count.index]}-role*" {
        capabilities = ["read", "list"]
    }

    path "secret/metadata/${var.apps[count.index]}-creds*" {
      capabilities = ["read", "list"]
    }


    path "auth/*"
    {
      capabilities = ["create", "read", "update", "delete", "list", "sudo"]
    }

    EOT
}

resource "vault_policy" "pki-policy" {
  count = length(var.apps)
  name  = "pki-policy"

  policy = <<EOT

    path "sys/mounts/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
  }

# List enabled secrets engine
path "sys/mounts" {
  capabilities = [ "read", "list" ]
  }

# Work with pki secrets engine
path "pki*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
  }

    EOT
}
