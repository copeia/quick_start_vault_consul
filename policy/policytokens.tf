#token with app2Policy attached
resource "vault_token" "apps-token" {
  count        = length(var.apps)
  display_name = "${var.apps[count.index]}-policy"
  policies     = ["default", "${var.apps[count.index]}-policy"]
}
