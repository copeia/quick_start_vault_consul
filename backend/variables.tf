variable "token" {
  description = "Default Vault Token"
  default     = ""
}

variable "apps" {
  default = [
    "canary",
    "ilc",
    "new_app"]
}
