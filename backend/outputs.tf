output "db_mount" {
  value = "${vault_mount.db.path}"
}

output "db_name" {

  value = [
   for db in vault_database_secret_backend_connection.mssql:
   db.name
]

}
