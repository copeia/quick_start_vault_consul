terraform {
  backend "consul" {
    address = "localhost:8500"
    path    = "vault/engine"
  }
}

provider "vault" {
  # This will default to using $VAULT_ADDR
  # But can be set explicitly
  address = "http://localhost:8200"
  token   = "testing123"
  ## token = we reccommend to set this as an environment variable ex. TF_VAR_token
}


data "terraform_remote_state" "policy" {
  backend = "consul"
  config = {
    path = "vault/policy"
  }
}
