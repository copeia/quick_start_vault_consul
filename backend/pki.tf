resource "vault_pki_secret_backend" "pki" {
  path = "pki"
}

resource "vault_pki_secret_backend_crl_config" "crl_config" {
  backend   = "${vault_pki_secret_backend.pki.path}"
  expiry    = "87600h"
  disable   = false
}

resource "vault_pki_secret_backend_root_cert" "root" {
  depends_on = [ "vault_pki_secret_backend_crl_config.crl_config" ]

  backend = "${vault_pki_secret_backend.pki.path}"

  type = "internal"
  common_name = "qvc.com"
  ttl = "87600h"
  format = "pem"
  private_key_format = "der"
  key_type = "rsa"
  key_bits = 4096
  exclude_cn_from_sans = true
  ou = "My OU"
  organization = "QVC"
}

resource "vault_pki_secret_backend_config_urls" "config_urls" {
  depends_on = [ "vault_pki_secret_backend_root_cert.root" ]
  backend              = "${vault_pki_secret_backend.pki.path}"
  issuing_certificates = ["http://127.0.0.1:8200/v1/pki/ca"]
  crl_distribution_points = ["http://127.0.0.1:8200/v1/pki/crl"]
}

resource "vault_pki_secret_backend_role" "role" {
  depends_on = [ "vault_pki_secret_backend_config_urls.config_urls" ]
  count   = length(data.terraform_remote_state.policy.outputs.apps)
  backend = "${vault_pki_secret_backend.pki.path}"
  name    = "${data.terraform_remote_state.policy.outputs.apps[count.index]}-role"
  allow_localhost = true
  allow_any_name = true
  ttl= "72h"
}

resource "vault_pki_secret_backend_cert" "apps" {
  depends_on = [ "vault_pki_secret_backend_role.role" ]
  count   = length(data.terraform_remote_state.policy.outputs.apps)
  backend = "${vault_pki_secret_backend.pki.path}"
  name = "${vault_pki_secret_backend_role.role[count.index].name}"
  common_name = "${data.terraform_remote_state.policy.outputs.apps[count.index]}"
}

resource "vault_pki_secret_backend" "pki_int" {
  count = length(data.terraform_remote_state.policy.outputs.apps)
  path  = "${data.terraform_remote_state.policy.outputs.apps[count.index]}_pki_int"
}

resource "vault_pki_secret_backend_crl_config" "crl_config_int" {
  count = length(data.terraform_remote_state.policy.outputs.apps)
  backend   = "${vault_pki_secret_backend.pki_int[count.index].path}"
  expiry    = "43800h"
  disable   = false
}

resource "vault_pki_secret_backend_config_urls" "config_urls2" {
  depends_on = [ "vault_pki_secret_backend_crl_config.crl_config_int" ]
  count = length(data.terraform_remote_state.policy.outputs.apps)
  backend              = "${vault_pki_secret_backend.pki_int[count.index].path}"
  issuing_certificates = ["http://127.0.0.1:8200/v1/\"${data.terraform_remote_state.policy.outputs.apps[count.index]}_pki_int\"/ca"]
  crl_distribution_points = ["http://127.0.0.1:8200/v1/\"${data.terraform_remote_state.policy.outputs.apps[count.index]}_pki_int\"/crl"]
}

resource "vault_pki_secret_backend_intermediate_cert_request" "intermediate" {
  depends_on = [ "vault_pki_secret_backend_crl_config.crl_config_int" ]
  count   = length(data.terraform_remote_state.policy.outputs.apps)
  backend = "${vault_pki_secret_backend.pki_int[count.index].path}"

  type = "internal"
  common_name = "${data.terraform_remote_state.policy.outputs.apps[count.index]}-Intermediate"
}

resource "vault_pki_secret_backend_root_sign_intermediate" "root" {
  depends_on = [ "vault_pki_secret_backend_intermediate_cert_request.intermediate" ]
  count   = length(data.terraform_remote_state.policy.outputs.apps)
  backend = "${vault_pki_secret_backend.pki.path}"

  csr = "${vault_pki_secret_backend_intermediate_cert_request.intermediate[count.index].csr}"
  common_name = "${data.terraform_remote_state.policy.outputs.apps[count.index]}-Intermediate"
  format = "pem_bundle"
}

resource "vault_pki_secret_backend_intermediate_set_signed" "intermediate" { 
  depends_on = [ "vault_pki_secret_backend_root_sign_intermediate.root" ]
  count   = length(data.terraform_remote_state.policy.outputs.apps)
  backend = "${vault_pki_secret_backend.pki_int[count.index].path}"

  certificate = "${vault_pki_secret_backend_root_sign_intermediate.root[count.index].certificate}"
}

