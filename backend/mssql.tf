resource "vault_mount" "db" {
  path = "mssql4"
  type = "database"
}

# this needs to use REMOTE STATE to lookup apps
resource "vault_database_secret_backend_connection" "mssql" {
  count         = length(var.apps)
  backend       = "${vault_mount.db.path}"
  name          = "${var.apps[count.index]}-mssql"
  allowed_roles = [format("%s-role", "${var.apps[count.index]}")]
  //["${var.apps[count.index]}-role"]

  mssql {
    connection_url = "sqlserver://sa:Testing123@192.168.0.108:1433"

  }
}

